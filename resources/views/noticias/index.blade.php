@extends('noticias.layout')
 
@section('content')
    <div class="row">
        <div class="col-md-6">
            <h2>Listado de Noticias</h2>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-info" href="{{route('noticias.create')}}"> Crear Noticia </a>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-striped">
        <tr>
            <th width="280px">Noticia</th>
            <th>Descripción</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($noticias as $noticia)
        <tr>
            <td class="text-center">
                <img src="data:image/gif;base64,{{$noticia->fotografia}}" class="img-fluid" alt="Responsive image">
                <br>
                <strong>{{ $noticia->titulo }}</strong>
            </td>
            <td class="align-middle">
                <p>{{ $noticia->descripcion }}</p>            
            </td>
            <td class="align-middle">
                <form action="{{ route('noticias.destroy',$noticia->id) }}" method="POST">

    
                    <a class="btn btn-info" href="{{ route('noticias.edit',$noticia->id) }}">Modificar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $noticias->links() !!}
      
@endsection