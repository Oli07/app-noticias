@extends('noticias.layout')
   
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <h2>Editar Noticia</h2>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('noticias.update',$noticia->id) }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Título:</label>
                    <input type="text" value="{{ $noticia->titulo }}" name="titulo" class="form-control" placeholder="Título" required>
                    <div class="invalid-feedback">
                        Campo obligatorio
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                <label>Autor</label>
                <select class="form-control" name="autor_id" required> 
                    @foreach($autores as $autor)
                        @if($autor->id === $noticia->autor_id)
                            <option selected value="{{$autor->id}}">{{$autor->nombre}} {{$autor->apellido_paterno}} {{$autor->apellido_materno}}</option>
                        @else
                            <option value="{{$autor->id}}">{{$autor->nombre}} {{$autor->apellido_paterno}} {{$autor->apellido_materno}}</option>
                        @endif
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    Campo obligatorio
                </div>
              </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Descripción</label>
                    <textarea class="form-control" name="descripcion" rows="3" required>{{$noticia->descripcion}}</textarea>
                    <div class="invalid-feedback">
                        Campo obligatorio
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary">Editar</button>
                <a class="btn btn-info" href="{{ route('noticias.index') }}"> Regresar</a>
            </div>
        </div>
    </form>
@endsection

<script>
(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>