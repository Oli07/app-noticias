<?php

namespace App\Http\Controllers;

use App\Noticia;
use App\Autor;
use Illuminate\Http\Request;
use Log;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::latest()->paginate(5);
  
        return view('noticias.index',compact('noticias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $autores = Autor::all();
        return view('noticias.create', compact('autores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required'
        ]);

        $path = $request->fotografia->storeAs('fotografías', $request->fotografia->getClientOriginalName());
        Log::debug("Path ".$path);
        
        Log::debug("Request ".$request);
        $noticia = new Noticia($request->all());
       
        $noticia->fotografia = base64_encode(file_get_contents($request->file('fotografia')->path()));
        $noticia->save();
   
        return redirect()->route('noticias.index')
                        ->with('success','Noticia creada con éxito.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function edit(Noticia $noticia)
    {
        $autores = Autor::all();
        return view('noticias.edit',compact('noticia', 'autores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Noticia $noticia)
    {
        $request->validate([
            'titulo' => 'required'
        ]);
  
        $noticia->update($request->all());
  
        return redirect()->route('noticias.index')
                        ->with('success','Noticia actualizada con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Noticia  $noticia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Noticia $noticia)
    {
        $noticia->delete();
  
        return redirect()->route('noticias.index')
                        ->with('success','Noticia eliminada con éxito.');
    }
}
